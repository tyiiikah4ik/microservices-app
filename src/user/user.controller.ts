import "reflect-metadata";
import { Controller, Post, Body } from "@nestjs/common";
import { UserService } from "./user.service";
import { userDTO } from "./dto/user.dto";

@Controller("/users")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post("/create")
  async createUser(@Body() data: userDTO) {
    console.log(data);
    return await this.userService.createUser(data);
  }
}
