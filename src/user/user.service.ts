import "reflect-metadata";
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { User } from "./user.entity";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>
  ) {}

  async createUser(userData): Promise<User> {
    const userExistsCheck = await this.userRepository.findOneBy({
      email: userData.email,
    });
    if (!userExistsCheck) {
      const user = new User();
      user.email = userData.email;
      user.password = userData.password;
      user.name = userData.name;
      user.createdAt = new Date();
      return await this.userRepository.save(user);
    } else {
      throw new HttpException("User already exists", HttpStatus.FORBIDDEN);
    }
  }
}
