import "reflect-metadata";
import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 200,
    unique: true,
  })
  email: string;

  @Column({
    type: "varchar",
    length: 100,
  })
  password: string;

  @Column({
    type: "varchar",
    length: 100,
  })
  name: string;

  @Column({
    type: "timestamptz",
  })
  createdAt: Date;
}
